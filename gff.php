<?php 
$folderPath = 'feeds';
define('SERVER', 'localhost');
define('USER', 'root');
define('PASS', '');
define('DB', 'panamerica');

$file = getFile($folderPath);


if($file){
	
	if (($handle = fopen($file, "r")) !== FALSE) {
		$all_rows = array();
		$header = null;
	  while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
		if ($header === null) {
			$header = $data;
			continue;
		}
		$all_rows[] = array_combine($header, $data);
	  }
	  // close file handle
	  fclose($handle);
		
	}
	// save data into database
		saveData($all_rows);
		archiveFile($file);
	
	
}else{
	echo "File not found."; exit;
}

function saveData($data){
	$dbCon = createDbConnection();
	foreach($data as $row){
		$keys = '';
		$values = '';
		foreach($row as $k=> $val){
			$keys .= "`".$k."`,";
			$values .= "'".$val."',";
		}
		$keys = rtrim($keys, ',');
		$values = rtrim($values, ',');
		$qry = "INSERT INTO gff ({$keys}) VALUES ({$values});";
		if(! $dbCon->query($qry)){
			echo "Error: " . $dbCon->error  . "<br />";
		}
	}
	// close connection
	closeDbConnection($dbCon);	
	
}



function getFile($path){

$latest_ctime = 0;
$latest_filename = false;    

$d = dir($path);
	while (false !== ($entry = $d->read())) {
	  $filepath = "{$path}/{$entry}";
	  // could do also other checks than just checking whether the entry is a file
	  if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
		  $ext = pathinfo($filepath, PATHINFO_EXTENSION);
		  if($ext == 'csv'){
			$latest_ctime = filectime($filepath);
			$latest_filename = $path . '/' .$entry;
		  }
	  }
	}
	return $latest_filename;
}

function archiveFile($file){
	$filePath = explode('/', $file);
	$fl = end($filePath);
	rename($file, $filePath[0]."/archive/". $fl);
}

function createDbConnection(){
	$conn = new mysqli(SERVER, USER, PASS, DB);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}else{
		return $conn;
	}
}

function closeDbConnection($conn){
	$conn->close();
}

?>